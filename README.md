This demo demonstrates procedurally rigging an `R15` `Humanoid` in Roblox.

Usually this is done with the help of an animation plugin, to my knowlege there is no 
other complete attempt at this on the web.


Please look at the the `.lua` scripts in `Scripts/ModuleScripts` as that is where 
the majority of the work is, it is intended to be used modularly.

-- Make variables for Roblox services
local RunService = game:GetService("RunService")
local Players = game:GetService("Players")

-- Make variable for the camera offset
local OFFSET = Vector3.new(20,20,20)

-- Make variables for the player and camera
local player = Players.LocalPlayer
local camera = game.Workspace.CurrentCamera

-- Detatch the character's rotation from the camera
UserSettings().GameSettings.RotationType = Enum.RotationType.MovementRelative

-- Function to call on the render step
local function onRenderStep()
	-- Check if the player's character exists and if that character'script
	-- HumanoidRootPart exists
	if player.Character ~= nil then
		if player.Character:WaitForChild( "AsPlayerScript", 1 ) ~= nil then
			local character = player.Character
			if character then
				local humanoidRootPart = character:FindFirstChild( "HumanoidRootPart" )
				if humanoidRootPart then
					-- Update the position of the camera
					local playerPosition = humanoidRootPart.Position
					local cameraPosition = playerPosition + OFFSET
					camera.CoordinateFrame = CFrame.new(cameraPosition, playerPosition)

					-- Update the focus of the camera to follow the character
					camera.Focus = humanoidRootPart.CFrame
				end
			end
		end
	end
end

-- Binds function to render step at camera priority
RunService:BindToRenderStep("Camera", Enum.RenderPriority.Camera.Value, onRenderStep)

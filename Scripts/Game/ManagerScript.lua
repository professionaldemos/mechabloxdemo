local ReplicatedStorage = game:GetService( "ReplicatedStorage" )
local Rig15Humanoid = require( ReplicatedStorage:WaitForChild( "Rig15Humanoid" ) )
local Prefab = require( ReplicatedStorage:WaitForChild( "Prefab" ) )
local Utility = require( ReplicatedStorage:WaitForChild( "Utility" ) )
local ValueObjectManager = require( ReplicatedStorage:WaitForChild( "ValueObjectManager" ) )

local Players = game:GetService( "Players" )
local ServerStorage = game:GetService( "ServerStorage" )

local mech0Prefab = Rig15Humanoid.Initialize( ReplicatedStorage:WaitForChild( "Mech0Master" ), "Mech0" )

local players = {}

Players.PlayerAdded:Connect( function( player )
	player.CharacterAdded:Connect( function( charachter )
		if players[ player.Name ] == nil then
			local newPlayer = {
				player = player,
				mech = Rig15Humanoid.SpawnRig15Humanoid( mech0Prefab, 
						workspace:WaitForChild( "SpawnLocation" ).CFrame.Position ), 
				originalCharacter = charachter, 
				originalCharacterSetToPrefab = false
			}
			newPlayer.mech.AsPlayerScript.Disabled = false
			players[ player.Name ] = newPlayer
			newPlayer.mech:WaitForChild( "playerId" ).Value = tostring( player.UserId )
		else
			return nil
		end
	end )
end )

local module = {}

module.Utility = require( script.Parent.Utility )
module.ValueObjectManager = require( script.Parent.ValueObjectManager )
module.AttributeTree = require( script.Parent.AttributeTree )
module.Prefab = require( script.Parent.Prefab )

module.HUMANOID_ROOT_PART_NAME = "HumanoidRootPart"
module.WELD_CONSTRAINT_TYPE_NAME = "WeldConstraint"
module.JOINT_NAME_SEPERATOR = "To"
module.MOTOR_6D_TYPE_NAME = "Motor6D"
module.RIG_TYPE_VS_RIG_MODIFIER_DELIMINATOR = "x"
module.HUMANOID_TYPE_NAME = "Humanoid"
module.DEFAULT_HUMANOID_MODEL_NAME = "NamelessRig15Humanoid"

module.DEFAULT_JOINT_MODIFICATION_FUNCTION_DEFAULT_JOINT_MAX_VELOCITY = .01

module.DEFAULT_WAIT_FOR_CHILD_TIMEOUT = 10

module.MODLE_VALUE_OBJECT_NAME = "Rig15HumanoidModel"
module.BODY_VALUE_OBJECT_NAME = "Rig15HumanoidBody"
module.R15_ATTRIBUTE_TREE_VALUE_OBJECT_NAME = "Rig15HumanoidR15AttributeTree"
module.ORIGINAL_PROPERTIES_VALUE_OBJECT_NAME = "Rig15HumanoidOriginalProperties"
module.MODEL_NAME_VALUE_OBJECT_NAME = "Rig15HumanoidModelName"
module.NUMBER_OF_MODEL_INSTANCES_SPAWNED_OBJECT_NAME = "Rig15HumanoidsSpawned"
module.INITIAL_NUMBER_OF_MODELS_SPAWNED = 0

function module.ObtainRig15HumanoidBody( rig15HumanoidModel )
	return rig15HumanoidModel:WaitForChild( module.HUMANOID_ROOT_PART_NAME )
end

function module.WeldRig15HumanoidBody( currentPart )
	local initialAnchoredState = currentPart.Anchored
	currentPart.Anchored = true
	local children = currentPart:GetChildren()
	for _, currentChild in ipairs( children ) do
		local newWeld = Instance.new( module.WELD_CONSTRAINT_TYPE_NAME, currentPart )
		newWeld.Part0 = currentPart
		newWeld.Part1 = currentChild
		module.WeldRig15HumanoidBody( currentChild )
	end
	currentPart.Anchored = initialAnchoredState
end

function module.InsertAttributeTreePartRefrences( currentPart, attributeTree )
	local partAttribute = currentPart:GetAttribute( attributeTree.name )
	local function SearchChildren( attributeToSearch )
		local children = currentPart:GetChildren()
		for _, currentChild in pairs( children ) do
			module.InsertAttributeTreePartRefrences( currentChild, attributeToSearch )
		end
	end
	if partAttribute == attributeTree.attribute then
		attributeTree.part = currentPart
		for _, childAttribute in pairs( attributeTree.children ) do
			SearchChildren( childAttribute )
		end
	else
		SearchChildren( attributeTree )
	end
end

function module.DefaultJointModificationFunction( jointData, defaultMaxVelocity )
	if defaultMaxVelocity == nil then
		defaultMaxVelocity = module.DEFAULT_JOINT_MODIFICATION_FUNCTION_DEFAULT_JOINT_MAX_VELOCITY
	end
	local modifyWeld = function( part0, part1, earlyBreak )
		local foundWeld = false
		local children = part0:GetChildren()
		for _, child in pairs( children ) do
			if child:IsA( module.WELD_CONSTRAINT_TYPE_NAME ) == true then
				if ( child.Part0 == part0 and child.Part1 == part1 ) then
					jointData.joint.C0 = ( jointData.parent.part.CFrame:inverse() * 
						jointData.child.part.CFrame )
					child.Enabled = false
					foundWeld = true
					if earlyBreak == true then
						break
					end
				end
			end
		end
		return foundWeld
	end
	if modifyWeld( jointData.parent.part, jointData.child.part, true ) == false then
		modifyWeld( jointData.child.part.Parent, jointData.child.part, true )
	end
	jointData.joint.MaxVelocity = defaultMaxVelocity
	return jointData
end

function module.CreateJoints( attributeTree, jointType, jointModificationFunction )
	if attributeTree.joints == nil then
		attributeTree.joints = {}
	end
	if jointModificationFunction  == nil then
		jointModificationFunction = module.DefaultJointModificationFunction
	end
	for childName, childAttribute in pairs( attributeTree.children ) do
		local newJoint = Instance.new( jointType, attributeTree.part )
		newJoint.Part1 = childAttribute.part
		newJoint.Part0 = attributeTree.part
		newJoint.Enabled = true
		attributeTree.joints[ jointType .. module.JOINT_NAME_SEPERATOR .. childName ] = 
				jointModificationFunction( {
					jointType = jointType, 
					joint = newJoint, 
					parent = attributeTree, 
					child = childAttribute
				} )
		module.CreateJoints( childAttribute, jointType, jointModificationFunction )
	end
end

function module.PlaceR156DMotors( rig15HumanoidModel )
	local r15Tree = module.AttributeTree.CreateR15AttributeTree()
	module.InsertAttributeTreePartRefrences( 
			module.ObtainRig15HumanoidBody( rig15HumanoidModel ), r15Tree )
	module.CreateJoints( r15Tree, module.MOTOR_6D_TYPE_NAME )
	return r15Tree
end

function module.ElevateParts( rig15HumanoidModel, attributeTree, depth )
	if depth == nil then
		attributeTree.part.Parent = rig15HumanoidModel
		attributeTree.name = 
			module.AttributeTree.PartNameStringFromPartIndex( attributeTree.nodeIndex )
	end
	for _, childAttribute in pairs( attributeTree.children ) do
		childAttribute.part.Parent = rig15HumanoidModel
		childAttribute.part.Name = 
			module.AttributeTree.PartNameStringFromPartIndex( childAttribute.nodeIndex )
		module.ElevateParts( rig15HumanoidModel, childAttribute )
	end
end

function module.InitializeRig15HumanoidModel( rig15HumanoidModel )
	local body = module.ObtainRig15HumanoidBody( rig15HumanoidModel )
	rig15HumanoidModel.PrimaryPart = body
	module.WeldRig15HumanoidBody( body )
	local r15Tree = module.PlaceR156DMotors( rig15HumanoidModel )
	module.ElevateParts( rig15HumanoidModel, r15Tree )
	script.Archivable = false
	return rig15HumanoidModel, body, r15Tree
end

function module.CaclulateFootHeight( footPart )
	return ( footPart.CFrame.Y + ( footPart.Size.Y / 2 ) )
end

--Helper function
function module.ObtainR15ModelBodyPartFromEnum( rig15HumanoidModel, bodyPartEnum, timeout )
	if timeout == nil then
		timeout = module.DEFAULT_WAIT_FOR_CHILD_TIMEOUT
	end
	return rig15HumanoidModel:WaitForChild( 
			module.Utility.EnumStemToString( bodyPartEnum ), timeout )
end

function module.FindR1g15HumanoidHumanoidHipHeight( rig15HumanoidModel, foot )
	local obtainFootHeight = function( foot )
		return module.CaclulateFootHeight( 
				module.ObtainR15ModelBodyPartFromEnum( rig15HumanoidModel, foot ) )
	end
	local footHeight = 0
	if foot == nil then
		local leftFootHeight = obtainFootHeight( Enum.BodyPartR15.LeftFoot )
		local rightFootHeight = obtainFootHeight( Enum.BodyPartR15.RightFoot )
		if leftFootHeight < rightFootHeight then
			footHeight = leftFootHeight
		else
			footHeight = rightFootHeight
		end
	else
		footHeight = obtainFootHeight( foot )
	end
	if footHeight == nil then
		error( "Rig15Humanoid::FindR1g15HumanoidHumanoidHipHeightError: " ..
				" Failed to get foot height, is a part called " .. 
				module.Utility.EnumStemToString( Enum.BodyPartR15.LeftFoot ) .. 
				" or " .. module.Utility.EnumStemToString( Enum.BodyPartR15.RightFoot ) .. 
				" a direct descendendant of the Humanoid/human-like R15 model?" )
	end
	local lowerTorsoHeight = module.ObtainR15ModelBodyPartFromEnum( 
		rig15HumanoidModel, Enum.BodyPartR15.LowerTorso ).CFrame.Y
	return lowerTorsoHeight - footHeight
end

function module.SetHumanoidHipHeight( rig15Humanoid, foot, rig15HumanoidModel )
	if rig15HumanoidModel == nil then
		rig15HumanoidModel = rig15Humanoid.Parent
	end
	rig15Humanoid.HipHeight = module.FindR1g15HumanoidHumanoidHipHeight( rig15HumanoidModel, foot )
end

function module.CreateRig15HumanoidHumanoid( rig15HumanoidModel, rig15HumanoidHumanoid )
	if rig15HumanoidHumanoid == nil then
		rig15HumanoidHumanoid = Instance.new( module.HUMANOID_TYPE_NAME, rig15HumanoidModel )
	else
		rig15HumanoidHumanoid.Parent = rig15HumanoidModel
	end
	rig15HumanoidHumanoid.RigType = Enum.HumanoidRigType.R15
	rig15HumanoidHumanoid.RequiresNeck = false
	rig15HumanoidHumanoid.AutomaticScalingEnabled = false
	local errorLocationPrefix = "Rig15Humanoid0::CreateRig15HumanoidHumanoid::Error:"
	if rig15HumanoidHumanoid.RootPart == nil then
		error( errorLocationPrefix .. "Humanoid Root Part is nil!" )
	end
	for _, childPart in pairs( rig15HumanoidModel:GetChildren() ) do
		local r15PartType = module.AttributeTree.NameOfR15BodyPartEnum( 
				rig15HumanoidHumanoid:GetBodyPartR15( childPart ) )
		if childPart:IsA( module.Utility.BASE_PART_TYPE_NAME ) == true and 
				tostring( childPart ) ~= r15PartType and 
				childPart ~= rig15HumanoidHumanoid.RootPart then 
			error( errorLocationPrefix .. 
				"Humanoid R15 Part Type does not match part tested for part: " .. 
				tostring( childPart ) .. " got result: " .. r15PartType )
		end
	end
	module.SetHumanoidHipHeight( rig15HumanoidHumanoid )
	return rig15HumanoidHumanoid
end

function module.MakeRig15Humanoid( newRig15Humanoid, position, orientation,  
		rig15HumanoidModelName, numberOfRig15HumanoidsSpawned, fromHumanoid )
	local initialCFrame = CFrame.new( position.X, 
			newRig15Humanoid.PrimaryPart.CFrame.Y + position.Y, 
			position.Z )
	if orientation ~= nil then
		initialCFrame = ( initialCFrame * orientation )
	end
	newRig15Humanoid.Parent = workspace
	newRig15Humanoid:SetPrimaryPartCFrame( initialCFrame )
	local newRig15HumanoidHumanoid = module.CreateRig15HumanoidHumanoid( newRig15Humanoid, fromHumanoid )
	newRig15Humanoid.Name = rig15HumanoidModelName .. 
			module.RIG_TYPE_VS_RIG_MODIFIER_DELIMINATOR .. 
			numberOfRig15HumanoidsSpawned
	--newRig15Humanoid.MechScript.Disabled = false
	print( "New Rig15Humanoid " .. newRig15Humanoid.Name .. " spawed at location", 
			newRig15Humanoid:GetPrimaryPartCFrame().Position )
	return newRig15Humanoid
end

function module.Initialize( rig15HumanoidModel, rig15HumanoidModelName )
	if rig15HumanoidModelName == nil then
		rig15HumanoidModelName = module.DEFAULT_HUMANOID_MODEL_NAME
	end
	local rig15HumanoidModel, rig15HumanoidBody, rig15HumanoidR15Tree = 
			module.InitializeRig15HumanoidModel( rig15HumanoidModel )
	local rig15HumanoidPrefab = module.Prefab.SetToPrefab( rig15HumanoidModel )
	local valueObjects = module.ValueObjectManager.CreateValueObjectManagerForAllTypes( rig15HumanoidModel )
	rig15HumanoidPrefab.body = rig15HumanoidBody
	rig15HumanoidPrefab.rig15Tree = rig15HumanoidR15Tree
	rig15HumanoidPrefab.modelName = rig15HumanoidModelName
	rig15HumanoidPrefab.amountSpawned = module.INITIAL_NUMBER_OF_MODELS_SPAWNED
	return rig15HumanoidPrefab
end

function module.SpawnRig15Humanoid( rig15Humanoid, position, orientation, fromHumanoid )
	local instance = module.Prefab.CreateInstance( 
			rig15Humanoid,  
			function( newInstance )
				module.MakeRig15Humanoid( newInstance, position, orientation, 
						rig15Humanoid.modelName, 
					rig15Humanoid.amountSpawned, fromHumanoid )
				rig15Humanoid.amountSpawned += 1
		end )
	instance.Parent = workspace
	return instance
end

return module

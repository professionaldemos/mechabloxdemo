local module = {}

module.VALUE_NAME_EXTENSION = "Value"

module.BOOL_VALUE_OBJECT_TYPE_NAME = "Bool" .. module.VALUE_NAME_EXTENSION
module.BRICK_COLOR_VALUE_OBJECT_TYPE_NAME = "BrickColor" .. module.VALUE_NAME_EXTENSION
module.CFRAME_VALUE_OBJECT_TYPE_NAME = "CFrame" .. module.VALUE_NAME_EXTENSION
module.COLOR_3_VALUE_OBJECT_TYPE_NAME = "Color3" .. module.VALUE_NAME_EXTENSION
module.DOUBLE_CONSTRAINED_VALUE_OBJECT_TYPE_NAME = "DoubleConstrained" .. module.VALUE_NAME_EXTENSION
module.INT_CONSTRAINED_VALUE_OBJECT_TYPE_NAME = "IntConstrained" .. module.VALUE_NAME_EXTENSION
module.INT_VALUE_OBJECT_TYPE_NAME = "Int" .. module.VALUE_NAME_EXTENSION
module.NUMBER_VALUE_OBJECT_TYPE_NAME = "Number" .. module.VALUE_NAME_EXTENSION
module.OBJECT_VALUE_OBJECT_TYPE_NAME = "Object" .. module.VALUE_NAME_EXTENSION
module.RAY_VALUE_OBJECT_TYPE_NAME = "Ray" .. module.VALUE_NAME_EXTENSION
module.STRING_VALUE_OBJECT_TYPE_NAME = "String" .. module.VALUE_NAME_EXTENSION
module.VECTOR_3_VALUE_OBJECT_TYPE_NAME = "Vector3" .. module.VALUE_NAME_EXTENSION

module.ALL_VALUE_OBJECT_TYPE_NAMES = {
	module.BOOL_VALUE_OBJECT_TYPE_NAME, 
	module.BRICK_COLOR_VALUE_OBJECT_TYPE_NAME, 
	module.CFRAME_VALUE_OBJECT_TYPE_NAME, 
	module.COLOR_3_VALUE_OBJECT_TYPE_NAME, 
	module.DOUBLE_CONSTRAINED_VALUE_OBJECT_TYPE_NAME, 
	module.INT_CONSTRAINED_VALUE_OBJECT_TYPE_NAME, 
	module.INT_VALUE_OBJECT_TYPE_NAME, 
	module.NUMBER_VALUE_OBJECT_TYPE_NAME, 
	module.OBJECT_VALUE_OBJECT_TYPE_NAME, 
	module.RAY_VALUE_OBJECT_TYPE_NAME, 
	module.STRING_VALUE_OBJECT_TYPE_NAME, 
	module.VECTOR_3_VALUE_OBJECT_TYPE_NAME
}


function module.NewValueObject( valueType, to, name, object )
	local valueObject = Instance.new( valueType )
	valueObject.Name = name
	valueObject.Value = object
	valueObject.Parent = to
	return valueObject
end

function module.QueryValueObject( valueType, from, name )
	for _, currentChild in pairs( from:GetChildren() ) do
		if currentChild:IsA( valueType ) == true then
			if currentChild.Name == name then
				return currentChild
			end
		end
	end
	return nil
end

function module.FindValueObjectsNamesOfType( from, valueType )
	local valueObjects = {}
	for _, currentChild in pairs( from:GetChildren() ) do
		if currentChild:IsA( valueType ) == true then
			valueObjects[ #valueObjects + 1 ] = currentChild.Name
		end
	end
	return valueObjects
end

function module.CreateTypedValueObjectManager( valueType, applicable, valueName )
	local queryFunction = function()
		return module.QueryValueObject( valueType, applicable, valueName )
	end
	return {
		query = queryFunction, 
		queryValue = function()
			return queryFunction().Value
		end, 
		set = function( value )
			queryFunction().Value = value
		end
	}
end

function module.CreateValueObjectTypeManager( valueObjectType, applicable )
	local manager = {
		valueType = valueObjectType, 
		valueObjectNames = module.FindValueObjectsNamesOfType( applicable, valueObjectType )
	}
	for _, valueName in pairs( manager.valueObjectNames ) do
		manager[ valueName ] = module.CreateTypedValueObjectManager( valueObjectType, applicable, valueName )
	end
	manager.new = function( valueName, value )
		manager[ valueName ] = module.CreateTypedValueObjectManager( valueObjectType, applicable, valueName )
		manager.valueObjectNames[ #( manager.valueObjectNames ) + 1 ] = manager[ valueName ].Name
		return module.NewValueObject( valueObjectType, applicable, valueName, value )
	end
	return manager
end

--[[
	ValueObjectManager manages the ValueObjects on an object and organizes them by type.
	It allows you to create and query them, set them, and make them.
--]]
function module.CreateValueObjectManager( valueObjectTypes, applicable )
	local manager = {}
	for _, valueObjectType in pairs( valueObjectTypes ) do
		manager[ valueObjectType:gsub( module.VALUE_NAME_EXTENSION, "" ) ] = 
			module.CreateValueObjectTypeManager( valueObjectType, applicable )
	end
	return manager
end

function module.CreateValueObjectManagerForAllTypes( applicable )
	return module.CreateValueObjectManager( module.ALL_VALUE_OBJECT_TYPE_NAMES, applicable )
end

return module

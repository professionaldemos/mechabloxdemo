local module = {}

module.Utility = require( script.Parent.Utility )

module.TABLE_TYPE_NAME = "table"

module.LEFT = "Left"
module.RIGHT = "Right"
module.HORIZONTAL_SIDES = { 
	[ 1 ] = module.LEFT, 
	[ 2 ] = module.RIGHT, 
	LEFT = module.LEFT, 
	RIGHT = module.RIGHT, 
	LEFT_INDEX = 1, 
	RIGHT_INDEX = 2
}

module.UPPER = "Upper"
module.LOWER = "Lower"
module.VERTICAL_SIDES = {
	[ Enum.BodyPartR15.UpperTorso.Value ] = module.UPPER,
	[ Enum.BodyPartR15.LowerTorso.Value ] = module.LOWER, 
	UPPER = module.UPPER, 
	LOWER = module.LOWER, 
	UPPER_INDEX = Enum.BodyPartR15.UpperTorso.Value, 
	LOWER_IDNEX = Enum.BodyPartR15.LowerTorso.Value
}

module.SIDES = { 
	[ 1 ] = module.LEFT, 
	[ 2 ] = module.RIGHT, 
	[ 3 ] = module.UPPER, 
	[ 4 ] = module.LOWER,
	HORIZONTAL = module.HORIZONTAL_SIDES, 
	VERTICAL = module.VERTICAL_SIDES,
	LEFT = module.LEFT, 
	RIGHT = module.RIGHT, 
	UPPER = module.UPPER, 
	LOWER = module.LOWER
}


module.R15_HEAD = "R15Head"
module.R15_HEAD_INDEX = Enum.BodyPartR15.Head.Value

module.R15_TORSO = "R15Torso"
module.R15_TORSO_INDICIES = { 
	[ 1 ] = module.VERTICAL_SIDES.UPPER_INDEX, 
	[ 2 ] = module.VERTICAL_SIDES.LOWER_INDEX, 
	UPPER = module.VERTICAL_SIDES.UPPER_INDEX,
	LOWER = module.VERTICAL_SIDES.LOWER_INDEX
}

module.R15_SHOULDER = "R15Shoulder"
module.R15_ELBOW = "R15Elbow"
module.R15_HAND = "R15Hand"

module.R15_ARM_LIMB_PARTS = {
	[ 1 ] = module.R15_SHOULDER, 
	[ 2 ] = module.R15_ELBOW, 
	[ 3 ] = module.R15_HAND, 
	UPPER = module.R15_SHOULDER, 
	LOWER = module.R15_ELBOW, 
	END = module.R15_HAND
}

function module.FindR15ArmLimbIndex( horizontalSideIndex )
	return ( 3 * horizontalSideIndex ) + 6
end

module.R15_HIP_JOINT = "R15HipJoint"
module.R15_KNEE = "R15Knee"
module.R15_FOOT = "R15Foot"

module.R15_LEG_LIMB_PARTS = {
	[ 1 ] = module.R15_HIP_JOINT, 
	[ 2 ] = module.R15_KNEE, 
	[ 3 ] = module.R15_FOOT, 
	UPPER = module.R15_HIP_JOINT, 
	LOWER = module.R15_KNEE, 
	END = module.R15_FOOT
}

function module.FindR15LegLimbIndex( horizontalSideIndex )
	return 3 * horizontalSideIndex
end

function module.PartNameStringFromPartIndex( partIndex )
	return module.Utility.EnumStemToString(
				module.Utility.IntegerToEnum( Enum.BodyPartR15, 
				partIndex ) )
end

function module.NameOfR15BodyPartEnum( r15bodyPart )
	return module.Utility.FindStem( tostring( r15bodyPart ), module.Utility.ENUM_DELIMINATOR )
end

function module.CreateAttributeTreeNode( parent, nodeName, 
	nodeAttribute, nodeIndex, nodeChildren )
	if nodeAttribute == nil then
		nodeAttribute = ""
	end
	if nodeIndex == nil then
		if parent.nodeIndex ~= nil then
			nodeIndex = parent.nodeIndex + 1
		else
			nodeIndex = 0
		end
	end
	if not ( type( nodeChildren ) == module.TABLE_TYPE_NAME ) then
		nodeChildren = {}
	end
	local newNode = {
		name = nodeName,
		children = nodeChildren, 
		attribute = nodeAttribute, 
		nodeIndex = nodeIndex, 
		appendChild = function( self, nodeName, 
			nodeAttribute, nodeChildren )
			return module.CreateAttributeTreeNode( self.children, nodeName, 
				nodeAttribute, nodeChildren )
		end,
	}
	parent[ nodeName .. nodeAttribute ] = newNode
	return newNode
end

function module.CreateAttributeTree( rootNodeName, children )
	return module.CreateAttributeTreeNode( {}, rootNodeName, children )
end

function module.CreateR15AttributeTree()
	local r15Tree = module.CreateAttributeTree( module.R15_HEAD, "", module.R15_HEAD_INDEX )
	local upperTorsoAttribute = r15Tree:appendChild( module.R15_TORSO, 
			module.VERTICAL_SIDES.UPPER, module.VERTICAL_SIDES.UPPER_INDEX )
	local lowerTorsoAttribute = upperTorsoAttribute:appendChild( module.R15_TORSO, 
			module.VERTICAL_SIDES.LOWER, module.VERTICAL_SIDES.LOWER_IDNEX )
	local function CreateLimb( root, side, threePartLimbNames, startIndex )
		root:appendChild( threePartLimbNames.UPPER, side, startIndex + 2 ) : 
				appendChild( threePartLimbNames.LOWER, side, startIndex + 1 ) : 
				appendChild( threePartLimbNames.END, side, startIndex )
	end
	--Should only index values with integer indicies
	for sideIndex, side in ipairs( module.HORIZONTAL_SIDES ) do
		CreateLimb( upperTorsoAttribute, side, 
				module.R15_ARM_LIMB_PARTS, 
				module.FindR15ArmLimbIndex( sideIndex ) )
		CreateLimb( lowerTorsoAttribute, side, 
				module.R15_LEG_LIMB_PARTS, 
				module.FindR15LegLimbIndex( sideIndex ) )
	end
	return r15Tree
end

return module

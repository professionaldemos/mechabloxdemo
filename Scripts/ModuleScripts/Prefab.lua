local module = {}

module.Utility = require( script.Parent.Utility )

function module.SetToPrefab( masterModel )
	local originalProperties = {}
	module.Utility.EditPartProperties( masterModel, function( part )
		local partName = tostring( part )
		originalProperties[ partName ] = {}
		originalProperties[ partName ].Anchored = part.Anchored
		originalProperties[ partName ].Transparency = part.Transparency
		originalProperties[ partName ].Reflectance = part.Reflectance
		originalProperties[ partName ].CastShadow = part.CastShadow
		originalProperties[ partName ].CanCollide = part.CanCollide
		originalProperties[ partName ].CanTouch = part.CanTouch
		originalProperties[ partName ].Massless = part.Massless
		part.Anchored = true
		part.Transparency = 100
		part.Reflectance = 0
		part.CastShadow = false
		part.CanCollide = false
		part.CanTouch = false
		part.Massless = false
	end )
	return {
		prefab = masterModel, 
		originalProperties = originalProperties
	}
end

function module.CreateInstance( prefab, construct )
	local newInstance = prefab.prefab:Clone()
	module.Utility.EditPartProperties( newInstance, function( part )
		local partName = tostring( part )
		part.Anchored = prefab.originalProperties[ partName ].Anchored
		part.Transparency = prefab.originalProperties[ partName ].Transparency
		part.Reflectance = prefab.originalProperties[ partName ].Reflectance
		part.CastShadow = prefab.originalProperties[ partName ].CastShadow
		part.CanCollide = prefab.originalProperties[ partName ].CanCollide
		part.CanTouch = prefab.originalProperties[ partName ].CanTouch
		part.Massless = prefab.originalProperties[ partName ].Massless
	end )
	construct( newInstance )
	return newInstance
end

return module

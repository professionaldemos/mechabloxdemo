local module = {}

module.ValueObjectManager = require( script.Parent.ValueObjectManager )
module.Animations = require( script.Parent.Animations )
module.Utility = require( script.Parent.Utility )

function module.SetupAnimations( humanoid )
	local valueObjectManager = module.ValueObjectManager.CreateValueObjectManagerForAllTypes( script )
	local animator = module.Animations.CreateHumanoidAnimator( humanoid, true )
	--Load all the animations availible for the states
	for _, state in pairs( Enum.HumanoidStateType:GetEnumItems() ) do
		local success, animation = pcall( module.Animations.AnimationFromChildValueObject, 
			module.Utility.EnumStemToString( state ), valueObjectManager ) 
		if success == true then
			print( "Loaded animation for ", state )
			animator:AddAnimationState( state, animation )
		end
	end
	humanoid.Running:Connect( function( speed )
		if speed > 0 then
			animator:PlayCurrentAnimation()
		elseif animator.currentTrack ~= nil then
			animator.currentTrack:Stop( 1 )
		end
	end )
	humanoid.StateChanged:Connect( function( oldState, newState )
		if newState ~= Enum.HumanoidStateType.Running then
			animator:PlayCurrentAnimation()
		end
	end )
end

return module

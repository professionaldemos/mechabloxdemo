local module = {}

module.ValueObjectManager = require( script.Parent.ValueObjectManager )
module.Utility = require( script.Parent.Utility )
module.Rig15Humanoid = require( script.Parent.Rig15Humanoid )

module.KEYFRAME_SEQUENCE_TYPE_NAME = "KeyframeSequence"
module.ANIMATION_TYPE_NAME = "Animation"

module.HIP_HEIGHT_CHANGED_ANIMATION_TRACK_MARKER_SIGNAL = "HipHeightChanged"

module.KeyframeSequenceProvider = game:GetService( "KeyframeSequenceProvider" )

function module.KeyframesToAnimation( keyframes )
	local animation = Instance.new( module.ANIMATION_TYPE_NAME )
	animation.AnimationId = module.KeyframeSequenceProvider:RegisterKeyframeSequence( keyframes )
	return animation
end

function module.AnimationFromChildValueObject( animationName, valueObjectManager )
	local animationContainer = valueObjectManager.String[ animationName ].query()
	local animationData = animationContainer:FindFirstChildWhichIsA( module.KEYFRAME_SEQUENCE_TYPE_NAME, false )
	if animationData ~= nil  then
		return module.KeyframesToAnimation( animationData )
	else
		animationData = animationContainer:FindFirstChildWhichIsA( module.ANIMATION_TYPE_NAME, false )
		if animationData ~= nil then
			return animationData
		end
	end
	return nil
end


function module.CreateAnimator( animationController )
	return {
		animationController = animationController, 
		currentTrack = nil,
		currentState = nil, 
		--Allows this to be changed
		ProcessPreviousTrackState = function( self, nextState, nextTrack )
			if self.currentTrack ~= nil then
				self.currentTrack:Stop()
			end
			return self.currentTrack, self.currentState
		end,
		AddAnimationState = function( self, state, animation, animationFunction )
			if animationFunction == nil then
				animationFunction = function( lastStateInfo, state, animationTrack, animationController )
					animationTrack:Play()
				end
			end
			self[ state ] = {
				animation = animation, 
				animate = function()
					local nextTrack = self.animationController:LoadAnimation( self[ state ].animation )
					local previousTrack, previousState = self:ProcessPreviousTrackState( state, nextTrack )
					self.currentTrack = nextTrack
					self.currentState = state
					return animationFunction( {
								previousTrack = previousTrack, 
								previousState = previousState 
							}, state, self.currentTrack, self.animationController )
				end
			}
			return self[ state ]
		end
	}	
end

function module.CreateHumanoidAnimator( humanoid, animateHipHeight )
	local animator = module.CreateAnimator( humanoid )
	animator.currentState = Enum.HumanoidStateType.None
	animator.originaHipHeight = humanoid.HipHeight
	animator.PlayCurrentAnimation = function( self )
		local currentState = self.animationController:GetState()
		if self[ currentState ] ~= nil then
			self[ self.animationController:GetState() ].animate()
		end
		if animateHipHeight ~= nil and self.currentTrack ~= nil then
			self.currentTrack:GetMarkerReachedSignal( 
					module.HIP_HEIGHT_CHANGED_ANIMATION_TRACK_MARKER_SIGNAL ):Connect( function( markerName )
				module.Rig15Humanoid.SetHumanoidHipHeight( self.animationController )
			end )
			self.currentTrack.Stopped:Connect( function() 
				self.animationController.HipHeight = self.originaHipHeight
			end )
		end
	end

	return animator
end

print( "Loading Animations Module")

return module

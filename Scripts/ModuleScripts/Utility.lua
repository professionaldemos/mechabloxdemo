local module = {}

module.BASE_PART_TYPE_NAME = "BasePart"
module.ENUM_DELIMINATOR = "."

function module.IntegerToEnum( enum, integerValue )
	for enumIndex, enumValue in pairs( enum:GetEnumItems() ) do
		if ( enumIndex - 1 ) == integerValue then
			return enumValue
		end
	end
	return nil
end

function module.FindStem( path, deliminator )
	local begin, _ = path:reverse():find( "%" .. deliminator )
	return path:sub( path:len() - begin + 2 )
end

function module.EnumStemToString( enum )
	return module.FindStem( tostring( enum ), module.ENUM_DELIMINATOR )
end

function module.EditPartProperties( currentPart, editFunction )
	local children = currentPart:GetChildren()
	if currentPart:IsA( module.BASE_PART_TYPE_NAME ) == true then
		editFunction( currentPart )
	end
	for _, currentChild in ipairs( children ) do
		if currentChild:IsA( module.BASE_PART_TYPE_NAME ) == true then
			module.EditPartProperties( currentChild, editFunction )
		end
	end
end

function module.StartsWith( String, Start )
	return ( string.sub( String, 1, string.len( Start ) ) == Start )
end

return module

local Players = game:GetService( "Players" )
local ReplicatedStorage = game:GetService( "ReplicatedStorage" )
local MakeMechAnimations = require( ReplicatedStorage:WaitForChild( "MakeMechAnimations" ) )
local ValueObjectManager = require( ReplicatedStorage:WaitForChild( "ValueObjectManager" ) )
local Rig15Humanoid = require( ReplicatedStorage:WaitForChild( "Rig15Humanoid" ) )

local humanoid = script.Parent:WaitForChild( "Humanoid" )
MakeMechAnimations.SetupAnimations( humanoid )

local playerId = Instance.new( "StringValue", script.Parent )

playerId.Changed:Connect( function( playerId )
	for _, player in pairs( Players:GetPlayers() ) do
		if tostring( player.UserId ) == playerId then
			local originalCharacter = player.Character
			player.Character:WaitForChild( "Animate" ).Parent = script.Parent
			player.Character = script.Parent
			originalCharacter:Destroy()
			break
		end
	end 
end )

playerId.Name = "playerId"
